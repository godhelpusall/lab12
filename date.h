#ifndef _DATE_H_
#define _DATE_H_

#include <iostream>
#include <ostream>

class Date{

 private:
  int day;
  int month;
  int year;

 public:
  Date( int Day, int month, int year){
  this -> day = day;
  this -> month = month;
  this -> year = year;
  }
  
  int getDay() {return day;}
  int getMonth(){return month;}
  int getYear(){return year;}
  friend bool operator <(const Date& lhs, const Date& rhs);
  friend std::ostream& operator << (std::ostream& out, Date date);
};


#endif
